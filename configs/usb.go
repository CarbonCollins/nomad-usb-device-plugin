package configs

const (
	UsbBusDevPath     = "/dev/bus/usb/%03d/%03d"
	UsbVisibleDevices = "USB_VISIBLE_DEVICES"
	UsbDeviceId       = "USB_DEVICE_%d_ID"
)
