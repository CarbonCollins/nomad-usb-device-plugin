# Nomad USB Device Plugin

[![pipeline status](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/badges/main/pipeline.svg)](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/commits/main)
[![license](https://img.shields.io/badge/license-MPL--2.0-blue)](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/blob/main/LICENSE)
[![project status](https://img.shields.io/badge/status-experimental-orange)](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin)

A [device plugin](https://www.nomadproject.io/docs/devices) for fingerprinting USB devices in [Nomad](https://www.nomadproject.io/).

[[_TOC_]]

This plugin is currently experimental so the layout of the interfaces/configs may be subject to change
## Usage

In order to use this plugin you first need to add the compiled binary to the Nomad Plugins directory which by default is `[data_dir]/plugins` unless otherwise configured with the `plugin_dir` configuration key, More info can be found in the [Nomad docs for plugin_dir](https://www.nomadproject.io/docs/configuration#plugin_dir)

### Behavior

USB Device plugin using [google/gousb](https://github.com/google/gousb) to get data regarding available USB devices and will expose them via Fingerprint RPC to Nomad. 

Currently no statistics are created for these fingerprinted devices

### Configuration

There are two configurations that need to be applied. The first is the plugin stanza on the agent which allows you to configure fingerprinting and make devices available to use, and the seccond is the device stanza in your tasks to specify that you want to use the device.

#### Agent

| Placement | `plugin` |
| --------- | -------- |

The agent configuration currently provides the funcitonality to be able to include and exclude specific vendor codes from being fingerprinted and passed to Nomad

```hcl
plugin "usb" {
  config {
    included_vendor_ids = [0xc1f0]
    excluded_vendor_ids = []

    included_product_ids = [0x0030]
    excluded_product_ids = []

    fingerprint_period = "5s"
  }
}

```
valid configuration options are:

* `included_vendor_ids` (`[]uint16`): An array of VIDs to pass to nomad if found
* `excluded_vendor_ids` (`[]uint16`): An array of VIDs to not pass to nomad if found (this take priority over the include list)
* `included_product_ids` (`[]uint16`): An array of PIDs to pass to nomad if found
* `excluded_product_ids` (`[]uint16`): An array of PIDs to not pass to nomad if found (this take priority over the include list)
* `fingerprint_period` (`string`: `"1m"`): interval to repeat the fingerprint process to identify possible changes.
* `enable` (`bool`: `true`) Option to disable this plugin in config
* `mount_dev_nodes` (`bool`: `false`) Option to automatically mount the `/dev/bus/usb/xxx/yyy` node into containers requesting the device

#### Job

| Placement | `job -> group -> task -> resources -> device` |
| --------- | --------------------------------------------- |

The device stanza allows the standard `constraint` and `affinity` stanzas to specify what kind of usb device to use.

For any USB device you can use:
```hcl
device "usb" {}
```

But to specify a specific USB device you can use the VID/PID or any of the various attributes. As an example for a [ConBee II](https://phoscon.de/en/conbee2) USB device its vendor and product are:

| Type | Registered Name | ID (hex) | ID (dec) |
| --- | --- | --- | --- |
| Vendor | Dresden Elektronik | C1F0 | 7409 |
| Product | ZigBee gateway [ConBee II] | 0030 | 48 |


```hcl
device "c1f0/usb/0030" {}
```
which is equivilant to:

```hcl
device "usb" {
  constraint {
    attribute = "${device.vendor}"
    value = "c1f0"
  }

  constraint {
    attribute = "${device.model}"
    value = "0030"
  }
}
```

Or for picking a USB device based on its attributes you can do the following (Please note all {placeholder}_id values need to be decimal here)

```hcl
device "usb" {
  constraint {
    attribute = "${device.attr.vendor_id}"
    value = "7409"
  }

  constraint {
    attribute = "${device.attr.product_id}"
    value = "48"
  }
}
```

### Fingerprinted Attributes

| Attribute | Unit/Type | Description |
| --------- | --------- | ----------- |
| vendor | string | Human readable vendor name |
| product | string | Human readable product name |
| description | string | Human readable description of the USB device |
| classification | string | Human readable classification of the USB device |
| serial | string | Serial Number of the USB device |
| vendor_id | int | VID of the USB device |
| product_id | int | PID of the USB device |
| class_id | int | Class code for the USB device [see defined-class-codes](https://www.usb.org/defined-class-codes) |
| sub_class_id | int | Sub class code for the USB  (depends on class_id) [see defined-class-codes](https://www.usb.org/defined-class-codes) |
| protocol_id | int | protocol code for the USB device (depends on sub_class_id) [see defined-class-codes](https://www.usb.org/defined-class-codes) |

For human readable VID/PID values see [linux-usb.org usb-ids](http://www.linux-usb.org/usb-ids.html)

For class, sub class, and protocol see [usb.org defined-class-codes](https://www.usb.org/defined-class-codes)

## Development
### Requirements

- [Nomad](https://www.nomadproject.io/downloads.html) 0.9+
- [Go](https://golang.org/doc/install) 1.18 or later (to build the plugin)

### Building the USB device plugin

To build the binary of this plugin simple as the makefile provides a target for this:

```sh
$ make build
```

### Running the Plugin in Development

You can test this plugin (and your own device plugins) in development using the
[plugin launcher](https://github.com/hashicorp/nomad/tree/master/plugins/shared/cmd/launcher). The makefile provides
a target for this:

```sh
$ make eval
```

### Deploying Device Plugins in Nomad

Copy the plugin binary to the
[plugins directory](https://www.nomadproject.io/docs/configuration/index.html#plugin_dir) and
[configure the plugin](https://www.nomadproject.io/docs/configuration/plugin.html) in the client config. Then use the
[device stanza](https://www.nomadproject.io/docs/job-specification/device.html) in the job file to schedule with
device support.


## Limitations

Currently the usb driver does not automatically provide dev paths for the USB devices into the task, this means in containerized applications you will still have to supply the dev path manually e.g. `/dev/ttyUSB0` however this plugin does allow containerized tasks to only be deployed on nodes which have specified USB devices connected.

## Other Info

The `usb` device plugin is currently in beta and thus is not recommended for production use. This will remain the case until the current [limitations](#limitations) have been solved with the plugin. If you find any issues with the usb plugin please raise an issue on the plugins [issues](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues) page.

### Nomad Developer Documentation

Some of the documentation for this plugin is also included on the [Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad) site under the [USB community device plugin](https://developer.hashicorp.com/nomad/plugins/devices/community/usb). The information there is more geared towards using the plugin in Nomad as opposed to development of this plugin so make sure to check it out!

Please note that the documentation on the Nomad Developer site can be slightly out-of-date as changes are only released when Nomad does a release.

### USB Codes

For a full list of available class, subclass, and protocol codes you can see the [usb-if-defined-class-codes](https://www.usb.org/defined-class-codes) page on the usb

### USB Vendors & Products

There are several sources where you can find known vendor and product ids for USB devices. These are independently maintained lists from third parties but can be useful as a reference.

* [linux-usb.org](http://www.linux-usb.org/)
* [devicehunt.com](https://devicehunt.com/all-usb-vendors)

### Source Code & Compiled Binaries

The source code for this plugin can be found at [CarbonCollins/nomad-usb-device-plugin](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin). You will also find pre built binaries on the [releases](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/releases) page.

### Attributions

This code has been based on previous repositories from
[hashicorp/nomad-skeleton-device-plugin](https://github.com/hashicorp/nomad-skeleton-device-plugin)
and also from [cgbaker/hashitalk-2020-nomad-plugins](https://github.com/cgbaker/hashitalk-2020-nomad-plugins) as I am still getting to grips with golang, Nomad, and raw USB devices.
