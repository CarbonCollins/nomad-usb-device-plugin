// The outer plugin stanza is needed when running in a cluster.
// When developing the outer plugin stanza is not needed.

// A couple of test USB devices / examples for the vendor/product configurations:

//    Vendor:  `0x1CF1` "dresden elektronik ingenieurtechnik GmbH"
//    Product: `0x0030` "ConBee II"

//    Vendor:  `0x067b` "Prolific Technology, Inc"
//    Product:  `0x2303` "PL2303 Serial Port / Mobile Action MA-8910P"

// plugin "usb" {
  config {
    enabled = true
    mount_dev_nodes = true

    included_vendor_ids = [0x1CF1, 0x067b]
    excluded_vendor_ids = []

    included_product_ids = [0x0030, 0x2303]
    excluded_product_ids = []

    fingerprint_period = "10s"
  }
// }
