package usb

import (
	"fmt"

	"github.com/google/gousb"
	"github.com/google/gousb/usbid"
)

// Gets the Human readable Vender and Product descriptions from the usbid package.
func GetUsbidVendorAndProduct(desc *gousb.DeviceDesc) (vendor, product string) {
	vendor = fmt.Sprintf("unknown(%v)", desc.Vendor)
	product = fmt.Sprintf("unknown(%v)", desc.Product)

	if v, ok := usbid.Vendors[desc.Vendor]; ok {
		vendor = v.Name
		if p, ok := v.Product[desc.Product]; ok {
			product = p.Name
		}
	}

	return
}

// Gets the Human readable Class, SubClass, and Protocol descriptions from the usbid package.
func GetUsbidClassifications(desc *gousb.DeviceDesc) (class, subclass, protocol string) {
	class = fmt.Sprintf("unknown(%v)", desc.Class)
	subclass = fmt.Sprintf("unknown(%v)", desc.SubClass)
	protocol = fmt.Sprintf("unknown(%v)", desc.Protocol)

	if c, ok := usbid.Classes[desc.Class]; ok {
		class = c.Name
		if s, ok := c.SubClass[desc.SubClass]; ok {
			subclass = s.Name
			if p, ok := s.Protocol[desc.Protocol]; ok {
				protocol = p
			}
		}
	}

	return
}
