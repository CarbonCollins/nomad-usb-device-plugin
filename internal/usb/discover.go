package usb

import (
	"github.com/google/gousb"
)

type IDList = map[gousb.ID]struct{}

type DiscoveryOptions struct {
	ExcludedVendorIds IDList
	IncludedVendorIds IDList

	ExcludedProductIds IDList
	IncludedProductIds IDList
}

type DiscoveryDevice struct {
	Device *gousb.Device
	Serial string
}

// Discovers devices connected to the system and checks to see if they can be opened. This will take
// an exclusion and inclusion list into account to make sure only allowed devices are opened.
// All devices will be closed regardless of any errors.
func DiscoverDevices(options *DiscoveryOptions) (error, []DiscoveryDevice) {
	usbCtx := gousb.NewContext()
	defer usbCtx.Close()

	devices, err := usbCtx.OpenDevices(func(desc *gousb.DeviceDesc) bool {
		return isIdValid(options.ExcludedVendorIds, options.IncludedVendorIds, desc.Vendor) &&
			isIdValid(options.ExcludedProductIds, options.IncludedProductIds, desc.Product)
	})

	discoveredDevices := []DiscoveryDevice{}

	// all found USB devices are now open and need to be closed regardless if there is an error or not
	for _, device := range devices {
		serialNumber, _ := device.SerialNumber()

		discoveredDevices = append(discoveredDevices, DiscoveryDevice{
			Device: device,
			Serial: serialNumber,
		})
		defer device.Close()
	}

	if err != nil {
		return err, nil
	}

	return nil, discoveredDevices
}

// determins if the given USB ID is allowed to be opened based on an included and excluded list of
// ids. Excluded Ids take presidence over included ones.
func isIdValid(excludedIds IDList, includedIds IDList, id gousb.ID) bool {
	if _, excluded := excludedIds[id]; excluded {
		return false
	} else if len(includedIds) == 0 {
		return true
	} else if _, included := includedIds[id]; included {
		return true
	} else {
		return false
	}
}
