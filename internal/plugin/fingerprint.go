package plugin

import (
	"context"
	"fmt"
	"time"

	"github.com/google/gousb"
	"github.com/google/gousb/usbid"
	"github.com/hashicorp/nomad/helper/pointer"
	"github.com/hashicorp/nomad/plugins/device"
	"github.com/hashicorp/nomad/plugins/shared/structs"
	"gitlab.com/CarbonCollins/nomad-usb-device-plugin/internal/usb"
)

const (
	// Human Readable Attributes
	Vendor         = "vendor"
	Product        = "product"
	Description    = "description"
	Classification = "classification"
	Serial         = "serial"

	// Id Attributes
	VendorId   = "vendor_id"
	ProductId  = "product_id"
	ClassId    = "class_id"
	SubClassId = "sub_class_id"
	ProtocolId = "protocol_id"
)

// doFingerprint is the long-running goroutine that detects device changes
func (d *UsbDevicePlugin) doFingerprint(ctx context.Context, devices chan *device.FingerprintResponse) {
	defer close(devices)

	// Create a timer that will fire immediately for the first detection
	ticker := time.NewTimer(0)

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			ticker.Reset(d.fingerprintPeriod)
		}

		d.writeFingerprintToChannel(devices)
	}
}

// fingerprintedDevice is what we "discover" and transform into device.Device objects.
//
// plugin implementations will likely have a native struct provided by the corresonding SDK
type fingerprintedDevice struct {
	ID  string
	VID gousb.ID
	PID gousb.ID

	Vendor         string
	Product        string
	Description    string
	Classification string
	Serial         string

	Class    gousb.Class
	SubClass gousb.Class
	Protocol gousb.Protocol

	Bus     int
	Address int
	Port    int
}

// writeFingerprintToChannel collects fingerprint info, partitions devices into
// device groups, and sends the data over the provided channel.
func (d *UsbDevicePlugin) writeFingerprintToChannel(devices chan<- *device.FingerprintResponse) {
	discoveryOptions := &usb.DiscoveryOptions{
		ExcludedVendorIds: d.excludedVendorIds,
		IncludedVendorIds: d.includedVendorIds,

		ExcludedProductIds: d.excludedProductIds,
		IncludedProductIds: d.includedProductIds,
	}

	err, foundDevices := usb.DiscoverDevices(discoveryOptions)

	if err != nil {
		d.logger.Warn("error opening usb devices", "err", err)
		devices <- &device.FingerprintResponse{
			Devices: nil,
			Error:   err,
		}
		return
	}

	// prevent multiple instances of fingerprinting from modifying the discovered device list
	d.deviceLock.Lock()
	defer d.deviceLock.Unlock()

	discoveredDevices := make([]*fingerprintedDevice, 0, len(foundDevices))
	for _, foundDevice := range foundDevices {
		device := foundDevice.Device
		vendor, product := usb.GetUsbidVendorAndProduct(device.Desc)

		discoveredDevices = append(discoveredDevices, &fingerprintedDevice{
			ID:       generateUsbFingerprintId(device.Desc),
			VID:      device.Desc.Vendor,
			PID:      device.Desc.Product,
			Class:    device.Desc.Class,
			SubClass: device.Desc.SubClass,
			Protocol: device.Desc.Protocol,

			Bus:     device.Desc.Bus,
			Address: device.Desc.Address,
			Port:    device.Desc.Port,

			Vendor:         vendor,
			Product:        product,
			Description:    usbid.Describe(device.Desc),
			Classification: usbid.Classify(device.Desc),
			Serial:         foundDevice.Serial,
		})
	}

	if d.devices != nil && len(discoveredDevices) == len(d.devices) {
		var detectedDifference bool = false

		for _, discoveredDevice := range discoveredDevices {
			if _, notExist := d.devices[discoveredDevice.ID]; !notExist {
				detectedDifference = true
				break
			}
		}

		if !detectedDifference {
			d.logger.Info("no changes to devices")
			return
		}
	}

	d.devices = map[string]usb.Device{}

	// Group all FingerprintDevices by VID:PID or by VID:PID:SerialNumber if serial is present
	deviceListByDeviceName := make(map[string][]*fingerprintedDevice)
	for _, discoveredDevice := range discoveredDevices {
		deviceName := generateDeviceGroupName(discoveredDevice)
		deviceListByDeviceName[deviceName] = append(deviceListByDeviceName[deviceName], discoveredDevice)

		d.devices[discoveredDevice.ID] = usb.Device{
			// Common between devices of same type
			VID:      discoveredDevice.VID,
			PID:      discoveredDevice.PID,
			Class:    discoveredDevice.Class,
			SubClass: discoveredDevice.SubClass,
			Protocol: discoveredDevice.Protocol,

			// Device specific values
			Bus:     discoveredDevice.Bus,
			Address: discoveredDevice.Address,
			Port:    discoveredDevice.Port,
			Serial:  discoveredDevice.Serial,
		}
	}

	// Build Fingerprint response with computed groups and send it over the channel
	deviceGroups := make([]*device.DeviceGroup, 0, len(deviceListByDeviceName))
	for groupName, devices := range deviceListByDeviceName {
		deviceGroups = append(deviceGroups, deviceGroupFromFingerprintData(groupName, devices))
	}
	devices <- device.NewFingerprint(deviceGroups...)
}

// deviceGroupFromFingerprintData composes deviceGroup from a slice of detected devicers
func deviceGroupFromFingerprintData(groupName string, deviceList []*fingerprintedDevice) *device.DeviceGroup {
	// deviceGroup without devices makes no sense -> return nil when no devices are provided
	if len(deviceList) == 0 {
		return nil
	}

	devices := make([]*device.Device, 0, len(deviceList))
	for _, dev := range deviceList {
		devices = append(devices, &device.Device{
			ID:         dev.ID,
			Healthy:    true,
			HwLocality: nil,
		})
	}

	// Assumes all devices of the same vendor and product have the same attributes
	attributes := generateFingerprintAttributes(deviceList[0])

	deviceGroup := &device.DeviceGroup{
		Vendor:     deviceList[0].VID.String(),
		Type:       "usb",
		Name:       deviceList[0].PID.String(),
		Devices:    devices,
		Attributes: attributes,
	}

	return deviceGroup
}

// Generates a device group name allowing similar devices to be grouped together and share attributes
func generateDeviceGroupName(device *fingerprintedDevice) string {
	if len(device.Serial) != 0 {
		return fmt.Sprintf("%s:%s:%s", device.VID.String(), device.PID.String(), device.Serial)
	}

	return fmt.Sprintf("%s:%s", device.VID.String(), device.PID.String())
}

// Generates the attributes which will be assigned to a fingerprinted device
func generateFingerprintAttributes(device *fingerprintedDevice) map[string]*structs.Attribute {
	return map[string]*structs.Attribute{
		// Human Readable Attributes
		Vendor: {
			String: pointer.Of(device.Vendor),
		},
		Product: {
			String: pointer.Of(device.Product),
		},
		Description: {
			String: pointer.Of(device.Description),
		},
		Classification: {
			String: pointer.Of(device.Classification),
		},
		Serial: {
			String: pointer.Of(device.Serial),
		},

		// Classification Attributes
		VendorId: {
			Int: pointer.Of(int64(device.VID)),
		},
		ProductId: {
			Int: pointer.Of(int64(device.PID)),
		},
		ClassId: {
			Int: pointer.Of(int64(device.Class)),
		},
		SubClassId: {
			Int: pointer.Of(int64(device.SubClass)),
		},
		ProtocolId: {
			Int: pointer.Of(int64(device.Protocol)),
		},
	}
}

// Generates a unique identifier so that the plugin knows if a device has changed. This takes the
// Vendor and Product as well as where the device is connected on the host (Bus, Address, and Port)
func generateUsbFingerprintId(desc *gousb.DeviceDesc) string {
	return fmt.Sprintf("%03d-%03d-%03d-%04v-%04v", desc.Bus, desc.Address, desc.Port, desc.Vendor, desc.Product)
}
